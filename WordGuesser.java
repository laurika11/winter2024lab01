public class WordGuesser{
	
	//This method asks the user to enter a 4 letter word and then call the runGame method.
	public static void main(String[] args){
		java.util.Scanner reader = new java.util.Scanner(System.in);
		System.out.println("Please enter a 4 letter word with 4 different letter");
		String word = reader.next();
		runGame(word);
		
	}
	
	//This method takes a string representing a word and a char representing a letter.
	//It uses a for loop to check if the letter is in the word.
	//If it is, then it return the position in which the letter is in.
	//If it's not, then it returns -1.
	public static int isLetterInWord(String word, char c){
	
		for(int i = 0; i < word.length(); i++){
			char words = word.charAt(i);
			if(c == words){
				return i;
			}
		}
		
		return -1;
		
	}
	
	//This method takes a 4 letter word and booleans that represent if the letter is correct or not. 
	//If it's a correct letter then it prints the letter, but if it's not then it just print an underscore.
	//The letter or underscore is also added to a string that prints the result.
	public static void printWord(String word, boolean letter0, boolean letter1, boolean letter2, boolean letter3){
		
		String result = "Your result is ";

		if(letter0){
			result += word.charAt(0);
		}
		else{
			result += "_";
		}
		if(letter1){
			result += word.charAt(1);
		}
		else{
			result += "_";
		}
		if(letter2){
			result += word.charAt(2);
		}
		else{
			result += "_";
		}
		if(letter3){
			result += word.charAt(3);
		}
		else{
			result += "_";
		}
		System.out.print(result);
		
	}
	
	//This method takes a word and creates 4 booleans initiating them "false" at first.
	//It then uses a loop to ask the user to enter a letter.
	//In the loop it calls the isLetterInWord method, and then sets the boolean value of the position 
	//corresponding to the letter to true if the letter is in the word.
	//It prints the result each time after a letter is entered.
	//The loop stops when the user have guessed all the letters or when 6 mistakes have been made.
	public static void runGame(String word){
		boolean letter0 = false;
		boolean letter1 = false;
		boolean letter2 = false;
		boolean letter3 = false;
		
		int misses = 0;
		
		while(misses < 6 && !(letter0 && letter1 && letter2 && letter3)){
			java.util.Scanner read = new java.util.Scanner(System.in);
			System.out.println("Please enter a letter");
			final char c = read.next().charAt(0);
			
			if(isLetterInWord(word, c) == 0){
				letter0 = true;
			}
			else if(isLetterInWord(word, c) == 1){
				letter1 = true;
			}
			else if(isLetterInWord(word, c) == 2){
				letter2 = true;
			}
			else if(isLetterInWord(word, c) == 3){
				letter3 = true;
			}
			else{
			misses++;
			}
			
			printWord(word, letter0, letter1, letter2, letter3);
			System.out.println();
		}
		
	}
	
}